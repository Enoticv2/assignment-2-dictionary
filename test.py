import subprocess

# Функция для тестирования программы
def run_test(input_string):
    process = subprocess.Popen(
        "./app", 
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )
    out, err = process.communicate(input=input_string)
    return out, err

# Тест для поиска слова в словаре
def test_find_word():
    errors = ["Error!"]
    test_cases = [
        ["first_key", "first_word"],
        ["second_key", "second_word"],
        ["third_key", "third_word"],
    ]

    for test_case in test_cases:
        out, err = run_test(test_case[0])
        # print("out:",out, "err:", err)
        assert err not in errors
        assert test_case[1] in out


# Запуск тестов
if __name__ == "__main__":
    test_find_word()
    print("All tests passed successfully!")
